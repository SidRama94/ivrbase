package in.ac.iitb.ivrs.telephony.base.fsm.guards;

import in.ac.iitb.ivrs.telephony.base.IVRSession;
import in.ac.iitb.ivrs.telephony.base.events.GotDTMFEvent;

import com.continuent.tungsten.commons.patterns.fsm.Event;
import com.continuent.tungsten.commons.patterns.fsm.EventTypeGuard;
import com.continuent.tungsten.commons.patterns.fsm.State;

/**
 * Denotes the condition that is satisfied when the incoming event is GotDTMF and a desired key is pressed.
 */
public class OnGotDTMFKey extends EventTypeGuard<IVRSession> {

	String[] anyOf;
	boolean allow;

	/**
	 * Creates a new GotDTMF guard condition which will be true if the desired input is entered.
	 * @param anyOf The inputs for which this condition will evaluate to true if they are to be allowed. If
	 *              disallowed, any of these inputs will cause this condition to evaluate to false.
	 * @param allow Whether to allow the digits mentioned in anyOf or not (disallow). 
	 */
	public OnGotDTMFKey(String[] anyOf, boolean allow) {
		super(GotDTMFEvent.class);
		this.anyOf = anyOf;
		this.allow = allow;
	}

	/**
	 * Creates a new GotDTMF guard condition which will be true if the desired input is entered.
	 * @param anyOf The inputs for which this condition will evaluate to true. 
	 */
	public OnGotDTMFKey(String[] anyOf) {
		this(anyOf, true);
	}

	private boolean flipIfNeeded(boolean value) {
		if (allow)
			return value;
		else
			return !value;
	}

	@Override
	public boolean accept(Event<Object> event, IVRSession session, State<?> state) {
		if (super.accept(event, session, state)) {
			GotDTMFEvent ev = (GotDTMFEvent) event;
			for (String possibility : anyOf) {
				if (possibility.equals(ev.getInput())) {
					return flipIfNeeded(true);
				}
			}

			return flipIfNeeded(false);
		}

		return false;
	}

}