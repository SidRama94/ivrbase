package in.ac.iitb.ivrs.telephony.base.events;

import com.continuent.tungsten.commons.patterns.fsm.Event;

/**
 * A null event which represents 'no event'. This must be used when some action must be taken even without a triggering
 * event.
 */
public class NullEvent extends Event<Object> {

	public NullEvent() {
		super(null);
	}

}
