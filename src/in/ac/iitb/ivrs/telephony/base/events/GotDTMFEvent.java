package in.ac.iitb.ivrs.telephony.base.events;

import com.continuent.tungsten.commons.patterns.fsm.Event;

/**
 * Represents an event triggered by the user entering a keypress in response to a DTMF query.
 */
public class GotDTMFEvent extends Event<Object> {

	String data;

	/**
	 * Creates a new DTMF event.
	 * @param data A string containing the sequence of digits pressed by the user. If no input was provided, this
	 *             should be a blank string.
	 */
	public GotDTMFEvent(String data) {
		super(null);
		this.data = data;
	}

	/**
	 * Returns the keypress sequence of the DTMF event.
	 * @return A string containing the sequence of digits entered.
	 */
	public String getInput() {
		return data;
	}
}
