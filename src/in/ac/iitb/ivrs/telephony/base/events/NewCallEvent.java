package in.ac.iitb.ivrs.telephony.base.events;

import com.continuent.tungsten.commons.patterns.fsm.Event;

/**
 * Represents a new call event.
 */
public class NewCallEvent extends Event<Object> {

	public NewCallEvent() {
		super(null);
	}

}
